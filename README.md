e3-calc
===
ESS Site-specific EPICS module : calc

## Features
We enable the following two modules as default:

* SSCAN  for the swait record
* SNCSEQ for editSseq.st
